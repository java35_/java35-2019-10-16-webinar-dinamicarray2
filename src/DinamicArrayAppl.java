import mystatic.*;

public class DinamicArrayAppl {

	public static void main(String[] args) {
		ClassWithStaticField classWithStaticField = new ClassWithStaticField();
		ClassWithStaticField classWithStaticField1 = new ClassWithStaticField();
		
		classWithStaticField.printValues();
		System.out.println();
		classWithStaticField1.printValues();
		System.out.println("***************************************");
		classWithStaticField.changeValue(55);
		
		classWithStaticField.printValues();
		System.out.println();
		classWithStaticField1.printValues();
	}

}
