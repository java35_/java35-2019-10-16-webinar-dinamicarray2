package mystatic;

public class ClassWithStaticField {
			int valueNonStatic 	= 10;
	static 	int valueStatic 	= 100;
	
	public void printValues() {
		System.out.println("Non static: " 	+ valueNonStatic);
		System.out.println("Static: " 		+ valueStatic);
	}
	
	public void changeValue(int value) {
		valueNonStatic 	= value;
		valueStatic		= value;
	}
}
