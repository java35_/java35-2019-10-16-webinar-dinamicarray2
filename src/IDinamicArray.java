
public interface IDinamicArray {
	// Create
	public boolean add(Object obj);
	public boolean add(int index, Object obj);
	
	// Read
	public Object get(int index);
	public int size();
	public int indexOf(Object obj);
	public int lastIndexOf(Object obj);
	public boolean contains(Object obj);
	public int lenght();
	public boolean isEmpty();
	public Object[] toArray();		
	
	// Update
	public Object set(int index, Object obj);
	
	// Delete
	public boolean remove(Object obj);
	public Object remove(int index);
	public void clear();
	public boolean removeAll(DinamicArray array);
	boolean retainAll(DinamicArray array);
}